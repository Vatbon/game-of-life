package v.nevolin;

import java.util.Random;

public class Model {
    private int[][] cells;
    private int width;
    private int height;
    private int generation = 0;

    public Model(int width, int height) {
        this.width = width;
        this.height = height;
        cells = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int i1 = 0; i1 < height; i1++) {
                cells[i][i1] = Integer.MAX_VALUE;
            }
        }
    }

    public void setCells(int[][] cells) {
        this.cells = cells;
    }

    public int[][] getCells() {
        return cells;
    }

    public void step() {
        int[][] newState = new int[width][height];
        int[][] oldState = getCells();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int neighbours = countNeighboursAt(x, y);
                if (oldState[x][y] > 0) {
                    if (oldState[x][y] == Integer.MAX_VALUE) {
                        newState[x][y] = Integer.MAX_VALUE;
                    } else {
                        newState[x][y] = oldState[x][y] + 1;
                    }
                } else {
                    newState[x][y] = oldState[x][y];
                }
                if (oldState[x][y] > 0 && neighbours == 3) {
                    newState[x][y] = 0;
                } else if (oldState[x][y] == 0 && (neighbours < 2 || neighbours > 3)) {
                    newState[x][y] = 1;
                }
            }
        }
        generation++;
        setCells(newState);
    }


    private int countNeighboursAt(int x, int y) {
        int count = 0;
        int[][] curState = getCells();
        int left = Math.max(x - 1, 0);
        int right = Math.min(width - 1, x + 1);
        int top = Math.max(y - 1, 0);
        int bottom = Math.min(height - 1, y + 1);
        for (int i = left; i <= right; i++) {
            for (int j = top; j <= bottom; j++) {
                if (curState[i][j] == 0) {
                    count++;
                }
            }
        }
        if (curState[x][y] == 0) {
            count--;
        }
        return count;
    }

    public void initRandom() {
        generation = 0;
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        for (int i = 0; i < width; i++) {
            for (int i1 = 0; i1 < height; i1++) {
                cells[i][i1] = Integer.MAX_VALUE;
            }
        }
        for (int i = 0; i < width * height / 3; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            cells[x][y] = 0;
        }
    }

    public int getGeneration() {
        return generation;
    }
}
