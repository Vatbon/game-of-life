package v.nevolin;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

public class MainController {
    @FXML
    public AnchorPane cellPlate;
    @FXML
    public Button startButton;
    @FXML
    public Button randomButton;
    @FXML
    public Label generationLabel;
    @FXML
    public Canvas canvas;
    @FXML
    public ColorPicker colorPicker;
    @FXML
    public Slider decaySlider;
    @FXML
    public Slider delaySlider;

    private double initRed = 1;
    private double initGreen = 1;
    private double initBlue = 1;
    private double decayTimeline = 10;

    private int width = -1;
    private int height = -1;
    int[][] cells = null;
    Rectangle[][] rectangles = null;
    private final int SIZE = 10;
    private boolean init = true;

    public Integer getWidth() {
        if (width == -1) {
            width = (int) cellPlate.getPrefWidth() / SIZE;
        }
        return width;
    }

    public Integer getHeight() {
        if (height == -1) {
            height = (int) cellPlate.getPrefHeight() / SIZE;
        }
        return height;
    }

    private Color getColorByAge(int age) {
        if (age == 0) {
            return Color.WHITE;
        }
        double a1 = decayTimeline;
        double k1 = 0.3;
        //e^(-x / (0.3*x + 10))
        double r;
        double g;
        double b;
        r = g = b = Math.exp(-age / (k1 * age + a1));
        return Color.color(initRed * r, initGreen * g, initBlue * b);
    }

    public void setCells(int[][] cells) {
        setColor();
        this.cells = cells;
        canvas.setVisible(false);
        if (init) {
            cellPlate.getChildren().clear();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    Rectangle r = new Rectangle();
                    r.setHeight(SIZE);
                    r.setWidth(SIZE);
                    r.setLayoutX(x * SIZE);
                    r.setLayoutY(y * SIZE);
                    r.setStroke(Paint.valueOf("BLACK"));
                    r.setStrokeType(StrokeType.INSIDE);
                    r.setStrokeWidth(1);
                    r.setFill(getColorByAge(this.cells[x][y]));
                    r.setOnMouseEntered(event -> r.setFill(Paint.valueOf("GREY")));
                    int finalX = x;
                    int finalY = y;
                    r.setOnMouseExited(event -> {
                                r.setFill(getColorByAge(this.cells[finalX][finalY]));
                            }
                    );
                    r.setOnMouseClicked(event -> {
                        changeCell(finalX, finalY);
                    });
                    cellPlate.getChildren().add(r);
                }
            }
            init = false;
        }
        update();
        generationLabel.setText("Generation: " + AppMain.getGeneration());
    }

    private void update() {
        setColor();
        GraphicsContext context2D = canvas.getGraphicsContext2D();
        canvas.setVisible(true);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                context2D.setStroke(Color.BLACK);
                context2D.setFill(getColorByAge(this.cells[x][y]));
                context2D.strokeRect(x * SIZE, y * SIZE, SIZE, SIZE);
                context2D.fillRect(x * SIZE, y * SIZE, SIZE, SIZE);
            }
        }
    }

    private void changeCell(int finalX, int finalY) {
        if (cells != null) {
            if (cells[finalX][finalY] == 0)
                cells[finalX][finalY] = Integer.MAX_VALUE;
            else {
                cells[finalX][finalY] = 0;
            }
        }
        update();
    }

    public void start(ActionEvent actionEvent) {
        cellPlate.getChildren().clear();
        AppMain.startGame(cells, (int) delaySlider.getValue());
        startButton.setVisible(false);
    }

    public void randomStart(ActionEvent actionEvent) {
        cellPlate.getChildren().clear();
        AppMain.startRandom((int) delaySlider.getValue());
        startButton.setVisible(false);
    }

    @FXML
    private void setColor() {
        Color color = colorPicker.getValue();
        decayTimeline = decaySlider.getValue();
        initRed = color.getRed();
        initGreen = color.getGreen();
        initBlue = color.getBlue();
    }

    public void changeTimestep(MouseEvent keyEvent) {
        AppMain.changeTimeStep((int) delaySlider.getValue());
    }
}
