package v.nevolin;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.SneakyThrows;

import java.net.URL;

public class AppMain extends Application {

    static Model model;
    static MainController controller;
    static boolean isGameOn = false;
    static Timeline timeline;
    private static Image icon = new Image(AppMain.class.getResourceAsStream("/icon.png"));

    public static boolean getIsGameOn() {
        return isGameOn;
    }

    public static int getGeneration() {
        return model.getGeneration();
    }

    public static void startGame(int[][] cells, int value) {
        if (!isGameOn) {
            isGameOn = true;
            model.setCells(cells);
            Timeline timeline = new Timeline(new KeyFrame(Duration.millis(value), event -> {
                model.step();
                controller.setCells(model.getCells());
            }));
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.play();
        }
    }

    public static void startRandom(int value) {
        if (!isGameOn) {
            isGameOn = true;
            model.initRandom();
            timeline = new Timeline(new KeyFrame(Duration.millis(value), event -> {
                model.step();
                controller.setCells(model.getCells());
            }));
            timeline.setCycleCount(Timeline.INDEFINITE);
        } else {
            timeline.pause();
            model.initRandom();
        }
        timeline.play();
    }

    public static void changeTimeStep(int value) {
        if (isGameOn) {
            if (value < 20 || value > 1000) {
                return;
            }
            timeline.stop();
            timeline = new Timeline(new KeyFrame(Duration.millis(value), event -> {
                model.step();
                controller.setCells(model.getCells());
            }));
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.play();
        }
    }

    @SneakyThrows
    @Override
    public void start(Stage primaryStage) {
        FXMLLoader loader = new FXMLLoader();
        URL xmlUrl = AppMain.class.getResource("/main.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        controller = loader.getController();
        model = new Model(controller.getWidth(), controller.getHeight());
        int[][] cells = new int[controller.getWidth()][controller.getHeight()];
        for (int i = 0; i < controller.getWidth(); i++) {
            for (int i1 = 0; i1 < controller.getHeight(); i1++) {
                cells[i][i1] = Integer.MAX_VALUE;
            }
        }
        controller.setCells(cells);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.getIcons().add(icon);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
