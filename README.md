To launch an application run 
```shell script
mvn javafx:compile -f pom.xml
mvn javafx:run -f pom.xml
```

![look](https://i.imgur.com/rXoqrvPh.png)
